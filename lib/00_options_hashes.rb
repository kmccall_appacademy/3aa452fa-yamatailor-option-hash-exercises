# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(string, options = {})
  default = { times: 3, upcase: true, reverse: true }
  default_merge = default.merge(options)

  result = ""

  if default_merge[:upcase] == true
    result = string.upcase
  else
    result = string
  end

  if default_merge[:reverse] == true
    result = result.reverse
  end

  if default_merge.include?(:times)
    result *= default_merge[:times]
  end

  # default_merge.each do |k, v|
  #   if k == :upcase && v == true
  #     result.empty? ? result = string.upcase : result = result.upcase
  #   elsif k == :reverse && v == true
  #     result.empty? ? result = string.reverse : result = result.reverse
  #   elsif k == :times
  #     if result.empty?
  #       result = string * default_merge[:times]
  #     else
  #       result *= default_merge[:times]
  #     end
  #   end
  # end

  result
end
